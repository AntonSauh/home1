public class Balls {

    enum Color {green, red}

    public static void main(String[] param) {
        // for debugging
    }

    public static void reorder(Color[] balls) {
        int searchStartIndex = -1;
        for (int i = 0; i < balls.length; i++) {
            if (!balls[i].equals(Color.red)) {
                if (searchStartIndex == -1) {
                    searchStartIndex = i;
                }
                int nextRed = findNextRed(balls, searchStartIndex);
                if (nextRed != -1) {
                    searchStartIndex = nextRed;
                    swapBalls(i, nextRed, balls);
                } else {
                    break;
                }
            }
        }
    }

    private static void swapBalls(int i, int nextRed, Color[] balls) {
        Color redBall = balls[i];
        Color greenBall = balls[nextRed];
        balls[nextRed] = redBall;
        balls[i] = greenBall;
    }

    private static Integer findNextRed(Color[] balls, int startIndex) {
        startIndex++;
        while (startIndex < balls.length) {
            if (balls[startIndex].equals(Color.red)) {
                return startIndex;
            }
            startIndex++;
        }
        return -1;
    }
}

